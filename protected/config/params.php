<?php

// this contains the application parameters that can be maintained via GUI
return array(
	// this is displayed in the header section
	'title'=>'Kontakty',
	// this is used in error pages
	'adminEmail'=>'webmaster@example.com',
	// number of items displayed per page
	'perPage'=>10,
	// maximum number of comments that can be displayed in recent comments portlet
	'copyrightInfo'=>'Copyright &copy; 2015 by My Company.',
);
