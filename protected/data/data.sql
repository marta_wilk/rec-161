-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Czas generowania: 07 Wrz 2015, 12:07
-- Wersja serwera: 5.6.20
-- Wersja PHP: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `contacts`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
`id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `address` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `zip` varchar(10) NOT NULL,
  `is_friend` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Zrzut danych tabeli `contacts`
--

INSERT INTO `contacts` (`id`, `first_name`, `last_name`, `phone`, `email`, `address`, `city`, `zip`, `is_friend`) VALUES
(1, 'Imię 1', 'Nazwisko 1', '111320484', 'email1@wp.pl', 'Ulica 1', 'Miasto 1', '61048', 0),
(2, 'Imię 2', 'Nazwisko 2', '161439213', 'email2@wp.pl', 'Ulica 2', 'Miasto 2', '170780', 0),
(3, 'Imię 3', 'Nazwisko 3', '139476593', 'email3@wp.pl', 'Ulica 3', 'Miasto 3', '58623', 0),
(4, 'Imię 4', 'Nazwisko 4', '137928585', 'email4@wp.pl', 'Ulica 4', 'Miasto 4', '35592', 0),
(5, 'Imię 5', 'Nazwisko 5', '146150921', 'email5@wp.pl', 'Ulica 5', 'Miasto 5', '145987', 1),
(6, 'Imię 6', 'Nazwisko 6', '137222511', 'email6@wp.pl', 'Ulica 6', 'Miasto 6', '179198', 1),
(7, 'Imię 7', 'Nazwisko 7', '156912397', 'email7@wp.pl', 'Ulica 7', 'Miasto 7', '131438', 0),
(8, 'Imię 8', 'Nazwisko 8', '113724705', 'email8@wp.pl', 'Ulica 8', 'Miasto 8', '54403', 1),
(9, 'Imię 9', 'Nazwisko 9', '102452330', 'email9@wp.pl', 'Ulica 9', 'Miasto 9', '117019', 0),
(10, 'Imię 10', 'Nazwisko 10', '117544369', 'email10@wp.pl', 'Ulica 10', 'Miasto 10', '143895', 1),
(11, 'Imię 11', 'Nazwisko 11', '101598326', 'email11@wp.pl', 'Ulica 11', 'Miasto 11', '165404', 1),
(12, 'Imię 12', 'Nazwisko 12', '117610005', 'email12@wp.pl', 'Ulica 12', 'Miasto 12', '92138', 1),
(13, 'Imię 13', 'Nazwisko 13', '165719491', 'email13@wp.pl', 'Ulica 13', 'Miasto 13', '167144', 1),
(14, 'Imię 14', 'Nazwisko 14', '169868221', 'email14@wp.pl', 'Ulica 14', 'Miasto 14', '49208', 0),
(15, 'Imię 15', 'Nazwisko 15', '114836170', 'email15@wp.pl', 'Ulica 15', 'Miasto 15', '136895', 1),
(16, 'Imię 16', 'Nazwisko 16', '108177615', 'email16@wp.pl', 'Ulica 16', 'Miasto 16', '17617', 1),
(17, 'Imię 17', 'Nazwisko 17', '187807814', 'email17@wp.pl', 'Ulica 17', 'Miasto 17', '125751', 0),
(18, 'Imię 18', 'Nazwisko 18', '137552930', 'email18@wp.pl', 'Ulica 18', 'Miasto 18', '151828', 1),
(19, 'Imię 19', 'Nazwisko 19', '193704567', 'email19@wp.pl', 'Ulica 19', 'Miasto 19', '156488', 0);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `contacts`
--
ALTER TABLE `contacts`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
