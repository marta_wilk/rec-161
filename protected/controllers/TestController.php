<?php
/**
 * Created by PhpStorm.
 * User: rafal
 * Date: 07.09.15
 * Time: 11:49
 */

class TestController extends Controller {

    public function actionCreateContacts()
    {
        for($i = 1; $i < 20; $i++) {
            $model = new Contact();
            $model->setAttributes(array(
                'first_name' => 'Imię ' . $i,
                'last_name' => 'Nazwisko ' . $i,
                'phone' => rand(100000000, 200000000),
                'email' => 'email'.$i.'@wp.pl',
                'address' => 'Ulica ' . $i,
                'city' => 'Miasto ' . $i,
                'zip' => rand(10000, 200000),
                'is_friend' => rand(0,1)
            ));
            $model->save();
        }
    }
}