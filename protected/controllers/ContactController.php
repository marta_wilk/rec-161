<?php

class ContactController extends Controller
{

    public $defaultAction = 'admin';

    /**
     * Creates model
     */
    public function actionCreate()
    {
        $model = new Contact;

        $this->performAjaxValidation($model);

        if(Yii::app()->request->isAjaxRequest) {
            if (isset($_POST['Contact'])) {
                $model->attributes = $_POST['Contact'];
                if ($model->save()) {
                    Yii::app()->end();
                }
            }
            echo $this->renderPartial('_form', array('model' => $model), true, true);
            Yii::app()->end();
        } else throw new CHttpException(400);
    }

    /**
     * Updates model
     */
    public function actionUpdate($id)
    {
        $model = $this->loadModel($id);

        $this->performAjaxValidation($model);

        if(Yii::app()->request->isAjaxRequest) {
            if (isset($_POST['Contact'])) {
                $model->attributes = $_POST['Contact'];
                if ($model->save()) {
                    Yii::app()->end();
                }
            }
            echo $this->renderPartial('_form', array('model' => $model), true, true);
            Yii::app()->end();
        } else throw new CHttpException(400);
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new Contact('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Contact'])) {
            $model->attributes = $_GET['Contact'];
        }

        $this->render('admin', array(
            'model' => $model
        ));
    }

    /**
     * Deletes a selected models
      */
    public function actionDelete($id = null)
    {
        if($id) {
            $this->loadModel($id)->delete();
        } else {
            $ids = explode(',', Yii::app()->request->getPost('ids', ''));

            if (!empty($ids)) {
                foreach ($ids as $id) {
                    Contact::model()->deleteByPk($id);
                }
            }
        }

        if(!Yii::app()->request->isAjaxRequest) {
            $this->redirect(array('//contact/admin'));
        }

        Yii::app()->end();
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Contact the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model = Contact::model()->findByPk($id);
        if ($model === null) {
            throw new CHttpException(404, 'The requested page does not exist.');
        }
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Contact $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'contact-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
