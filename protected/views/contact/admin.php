<?php
/* @var $this ContactController */
/* @var $model Contact */

$this->breadcrumbs = array(
    Yii::t('contact', 'Contacts')
);
$this->pageTitle = Yii::t('contact', 'Contacts');
?>

<h1><?php echo Yii::t('contact', 'Manage contacts'); ?></h1>

<?php echo CHtml::link(Yii::t('site', 'Create'), array('//contact/create'), array('class' => 'create')); ?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'contact-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        array(
            'class'=>'CCheckBoxColumn',
            'id'=>'ids',
            'selectableRows' => '2',
        ),
        'first_name',
        'last_name',
        'phone',
        'email:email',
        'city',
        'zip',
        array(
            'name' => 'address',
            'footer' => Yii::t('contact', 'Friends:')
        ),
        array(
            'name' => 'is_friend',
            'filter' => array(
                '1' => Yii::t('site', 'Yes'),
                '0' => Yii::t('site', 'No'),
            ),
            'value' => function(Contact $model) {
                return $model->is_friend ? Yii::t('site', 'Yes') : Yii::t('site', 'No');
            },
            'footer' => $model->count('is_friend = 1')
        ),
        array(
            'class' => 'CButtonColumn',
            'template' => '{update}{delete}'
        ),
    ),

)); ?>
<?php
echo CHtml::ajaxLink(Yii::t('site', 'Delete selected rows'), array('//contact/delete'),
array(
    'type'=>'POST',
    'data'=>'js:{
        ids : $.fn.yiiGridView.getChecked("contact-grid","ids").toString()

        }',
    'success' => 'function(data){
    $("#contact-grid").yiiGridView("update");
    }'
)
   );
?>
<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
        'id' => 'contact-modal',
        'options' => array(
            'title' => Yii::t('contact', 'Contact'),
            'modal' => true,
            'autoOpen' => false,
            'width' => '600px'
        )
    )
);
$this->endWidget('zii.widgets.jui.CJuiDialog');
?>

<?php
Yii::app()->clientScript->registerScript('contact-modal', '

$("body").on("click",".update, .create",function(e){
        $.ajax({
                type:"GET",
                url:$(this).attr("href"),
                success:function(data){$("#contact-modal").dialog("open").html(data)},
                });
        return false;
        });

$("body").on("submit","#contact-form",function(e){
        $.ajax({
                type:"POST",
                data:$("#contact-form").serialize(),
                url:$("#contact-form").attr("action"),
                success:function(data){
                    if(data === ""){
                        $("#contact-modal").dialog("close").html("");
                        $("#contact-grid").yiiGridView("update")
                    } else {
                        $("#contact-modal").html(data);
                    }
                }
                });
        return false;
        });
');
