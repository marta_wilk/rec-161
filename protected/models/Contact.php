<?php

/**
 * This is the model class for table "contacts".
 *
 * The followings are the available columns in table 'contacts':
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $phone
 * @property string $email
 * @property string $address
 * @property string $city
 * @property string $zip
 * @property integer $is_friend
 */
class Contact extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'contacts';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('first_name, last_name, phone, email, address, city, zip', 'required'),
			array('is_friend', 'in', 'range' => array(0,1)),
			array('first_name, last_name', 'length', 'min' => 2, 'max'=>50),
			array('address, city', 'length', 'min' => 3, 'max'=>50),
			array('phone', 'length', 'min' => 9,'max'=>20),
			array('email', 'length', 'max'=>100),
			array('email', 'email'),
			array('zip', 'length', 'min'=>5, 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, first_name, last_name, phone, email, address, city, zip, is_friend', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('contact', 'ID'),
			'first_name' => Yii::t('contact', 'First name'),
			'last_name' => Yii::t('contact', 'Last name'),
			'phone' => Yii::t('contact', 'Phone'),
			'email' => Yii::t('contact', 'Email'),
			'address' => Yii::t('contact', 'Address'),
			'city' => Yii::t('contact', 'City'),
			'zip' => Yii::t('contact', 'Zip'),
			'is_friend' => Yii::t('contact', 'Is friend'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('zip',$this->zip,true);
		$criteria->compare('is_friend',$this->is_friend);


		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort' => array(
				'defaultOrder' => 't.id DESC'
			),
			'pagination' => array(
				'pageSize' => Yii::app()->params['perPage']
			)
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Contact the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
